package liquidacion.conceptos;

import java.math.BigDecimal;
import liquidacion.legajo.Empleado;

public interface ConceptoHaber {
    public BigDecimal calculo(Empleado empleado);
}
