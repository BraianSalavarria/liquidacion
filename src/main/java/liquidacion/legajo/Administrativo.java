
package liquidacion.legajo;

import java.time.LocalDate;

public class Administrativo extends Empleado{

    public Administrativo(Integer dni, String apellido, String nombre, Integer nroLegajo,LocalDate fechaIngreso) {
        super(dni, apellido, nombre, nroLegajo,fechaIngreso);
    }
    
}
