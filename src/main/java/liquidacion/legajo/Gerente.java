
package liquidacion.legajo;

import java.time.LocalDate;

public class Gerente extends Empleado{
    private String area;

    public Gerente(Integer dni, String apellido, String nombre, Integer nroLegajo,String area,LocalDate fechaIngreso) {
        super(dni, apellido, nombre, nroLegajo,fechaIngreso);
        this.area=area;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }
}
